# apt 2.0 backoport for bionic

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/apt-2.0-backport-for-bionic.asc https://packaging.gitlab.io/apt-2.0-backport-for-bionic/gpg.key
```

### Add repo to apt

```bash
. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/apt-2.0-backport-for-bionic ${DISTRIB_CODENAME} main" | sudo tee /etc/apt/sources.list.d/apt-2.0-backport-for-bionic.list
```
