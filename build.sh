#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
apt="${CI_COMMIT_TAG:-2.0.2ubuntu0.2}"
triehash="${TRIEHASH:-0.3-2}"
export DEBIAN_FRONTEND=noninteractive

. /etc/lsb-release

printf "\e[1;36mInstalling toolchain ...\e[0m\n"
apt-get -qq update
apt-get -t bionic-backports -qqy install debhelper
apt-get -qqy install \
    devscripts \
    equivs \
    ccache
export PATH="/usr/lib/ccache/bin/:$PATH"
export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR:-/tmp}/ccache}"
mkdir -p "$CCACHE_DIR"
tmpdir="$(mktemp -d)"
cd "${tmpdir}"
for file in "triehash_${triehash}.dsc" "triehash_${triehash%%-*}.orig.tar.gz" "triehash_${triehash}.debian.tar.xz"
do
    curl -sLO "http://archive.ubuntu.com/ubuntu/pool/universe/t/triehash/${file}"
done
dpkg-source -x "triehash_${triehash}.dsc"
mk-build-deps "triehash-${triehash%%-*}/debian/control"
apt-get -y install "./triehash-build-deps_${triehash}_all.deb"
rm -f "./triehash-build-deps_${triehash}_all.deb"
cd "triehash-${triehash%%-*}"
debuild \
    --preserve-envvar=CCACHE_DIR \
    --prepend-path=/usr/lib/ccache \
    --no-lintian \
    -us -uc \
    -j"$(getconf _NPROCESSORS_ONLN)"
cd -

for file in "apt_${apt}.dsc" "apt_${apt}.tar.xz"
do
    curl -sLO "http://archive.ubuntu.com/ubuntu/pool/main/a/apt/${file}"
done
dpkg-source -x "apt_${apt}.dsc"
mk-build-deps "apt-${apt}/debian/control"
apt-get -qqy install "./apt-build-deps_${apt}_amd64.deb" "./triehash_${triehash}_all.deb"
rm -f "./apt-build-deps_${apt}_amd64.deb" "./triehash_${triehash}_all.deb"
cd "apt-${apt}"
debuild \
    --preserve-envvar=CCACHE_DIR \
    --prepend-path=/usr/lib/ccache \
    --no-lintian \
    -us -uc \
    -j"$(getconf _NPROCESSORS_ONLN)"
uid="$(stat -c %u "${script_dir}"/build.sh)"
gid="$(stat -c %g "${script_dir}"/build.sh)"
install -o "${uid}" -g "${gid}" -m 644 "${tmpdir}"/*.deb "${script_dir}/"
