#!/bin/bash

set -e

mkdir -p "${CI_PROJECT_DIR}"/public
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/bionic/{pool,dists} \
  "${CI_PROJECT_DIR}"/public/
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/gpg.key \
  "${CI_PROJECT_DIR}"/public/
