#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

curl -sL "https://gitlab.com/packaging/utils/-/raw/${UTILS_BRANCH:-main}/repo.sh" \
    | bash -s -- bionic;
